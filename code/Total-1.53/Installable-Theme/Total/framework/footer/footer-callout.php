<?php
/**
 * Footer Callout
 *
 * @package Total
 * @subpackage Footer Functions
 * @author Alexander Clarke
 * @copyright Copyright (c) 2014, Symple Workz LLC
 * @link http://www.wpexplorer.com
 * @since Total 1.0
 */

/**
 * Checks if the footer callout is enabled or disabled
 *
 * @since Total 1.0
 * @return bool
 */
if ( ! function_exists( 'wpex_display_callout' ) ) {
	function wpex_display_callout() {
		
		// Callout is disabled in the admin so bye bye
		if ( ! wpex_option( 'callout', '1' ) ) {
			return false;
		}
		
		// Display callouts on posts/pages if custom field isn't checked
		if ( is_singular() ) {
			if ( 'on' == get_post_meta(get_the_ID(), 'wpex_disable_footer_callout', true ) ) {
				$return = false;
			} else {
				$return = true;
			}
		
		// For all else, display the callout
		} else {
			$return = true;
		}

		// Return and apply filters for easy child theme editing
		return apply_filters( 'wpex_disable_footer_callout', $return );
		
	}
}

/**
 * Outputs the footer callout HTML
 *
 * @since Total 1.0
 */
if ( ! function_exists( 'wpex_footer_callout' ) ) {
	function wpex_footer_callout() {
		
		// Lets bail if the callout is disabled for this page/post --> see previous function
		if ( ! wpex_display_callout() ) {
			return;
		}
		
		// Get theme options
		$callout_text = wpex_option( 'callout_text' );
		$callout_link = wpex_option( 'callout_link' );
		$callout_link_txt = wpex_option( 'callout_link_txt' );

		// Button rel
		if ( 'nofollow' == wpex_option( 'callout_button_rel', 'dofollow' ) ) {
			$rel = 'rel="nofollow"';
		} else {
			$rel = '';
		} ?>
			
		<div id="footer-callout-wrap" class="clr <?php echo wpex_option( 'callout_visibility' ); ?>">
			<div id="footer-callout" class="clr container">
				<div id="footer-callout-left" class="footer-callout-content clr <?php if ( ! wpex_option( 'callout_link' ) ) echo 'full-width'; ?>">
					<?php
					// Echo the footer callout text
					echo do_shortcode( $callout_text ); ?>
				</div><!-- #footer-callout-left -->
				<?php
				// Display footer callout button if callout link & text options are not blank in the admin
				if ( $callout_link && $callout_link_txt ) { ?>
					<div id="footer-callout-right" class="footer-callout-button clr">
						<a href="<?php echo $callout_link; ?>" class="theme-button footer-callout-button" title="<?php echo $callout_link_txt; ?>" target="_<?php echo wpex_option( 'callout_button_target', 'blank' ); ?>" <?php echo $rel; ?>><?php echo $callout_link_txt; ?></a>
					</div><!-- #footer-callout-right -->
				<?php } ?>
			</div><!-- #footer-callout -->
		</div><!-- #footer-callout-wrap -->
			
	<?php		
	}
}