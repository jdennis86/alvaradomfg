<?php
/**
 * Outputs your footer widgets
 *
 * @package Total
 * @subpackage Footer Functions
 * @author Alexander Clarke
 * @copyright Copyright (c) 2014, Symple Workz LLC
 * @link http://www.wpexplorer.com
 * @since Total 1.0
 */

if ( ! function_exists( 'wpex_footer_widgets' ) ) {
	function wpex_footer_widgets() {
		// Get footer widget columns option
		$footer_col = wpex_option( 'footer_col', '4' );
		// Output the footer widgets ?>
		<div id="footer-widgets" class="clr <?php if ( '1' == $footer_col ) echo 'single-col-footer'; ?>">
			<?php
			// Footer box 1 ?>
			<div class="footer-box <?php echo wpex_grid_class($footer_col); ?> col col-1">
				<?php dynamic_sidebar('footer_one'); ?>
			</div><!-- .footer-one-box -->
			<?php
			// Footer box 2
			if ( $footer_col > "1" ) { ?>
				<div class="footer-box <?php echo wpex_grid_class($footer_col); ?> col col-2">
					<?php dynamic_sidebar('footer_two'); ?>
				</div><!-- .footer-one-box -->
			<?php }
			// Footer box 3
			if ( $footer_col > "2" ) { ?>
				<div class="footer-box <?php echo wpex_grid_class($footer_col); ?> col col-3 ">
					<?php dynamic_sidebar('footer_three'); ?>
				</div><!-- .footer-one-box -->
			<?php }
			// Footer box 4
			if ( $footer_col > "3" ) { ?>
				<div class="footer-box <?php echo wpex_grid_class($footer_col); ?> col col-4">
					<?php dynamic_sidebar('footer_four'); ?>
				</div><!-- .footer-box -->
			<?php } ?>
		</div><!-- #footer-widgets -->
	<?php		
	}
}