<?php
/**
 * Outputs the scroll to top link into the wp_footer hook
 *
 * @package Total
 * @subpackage Footer Functions
 * @author Alexander Clarke
 * @copyright Copyright (c) 2014, Symple Workz LLC
 * @link http://www.wpexplorer.com
 * @since Total 1.1
 */

if ( ! function_exists( 'wpex_scroll_top' ) ) {
	function wpex_scroll_top() {
		// Don't do anything if disabled
		if ( ! wpex_option( 'scroll_top', '1' ) ) {
			return;
		}
		// If enabled echo the scroll to top link
		echo '<a href="#" id="site-scroll-top"><span class="fa fa-chevron-up"></span></a>';
	}
}
add_action( 'wp_footer', 'wpex_scroll_top' );