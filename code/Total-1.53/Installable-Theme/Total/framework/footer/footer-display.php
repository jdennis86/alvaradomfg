<?php
/**
 * Function used to show/hide the main footer depending on current post meta
 *
 * @package Total
 * @subpackage Footer Functions
 * @author Alexander Clarke
 * @copyright Copyright (c) 2014, Symple Workz LLC
 * @link http://www.wpexplorer.com
 * @since Total 1.0
 */

if ( ! function_exists( 'wpex_display_footer' ) ) {
	function wpex_display_footer() {

		// Always return true if not singular
		if ( !is_singular() ) {
			return true;
		}

		// Otherwise check if disabled on a per-post basis
		else {
			global $post;
			if ( 'on' == get_post_meta( $post->ID, 'wpex_disable_footer', true ) ) {
				return false;
			} else {
				return true;
			}
		}
		
	}
}

/**
 * Checks if the footer reveal option is enabled
 *
 * @since Total 1.0
 * @return bool
 */

if ( ! function_exists( 'wpex_footer_reveal_enabled' ) ) {
	function wpex_footer_reveal_enabled() {

		// Disable on mobile
		if ( wp_is_mobile() ) {
			return false;
		}

		// Disable on boxed style
		if ( 'boxed' == wpex_main_layout() ) {
			return false;
		}

		// Meta check
		if ( is_singular() ) {
			if ( 'on' == get_post_meta( get_the_ID(), 'wpex_footer_reveal', true ) ) {
				return true;
			} elseif ( 'off' == get_post_meta( get_the_ID(), 'wpex_footer_reveal', true ) ) {
				return false;
			}
		}

		// Theme option check
		if ( wpex_option( 'footer_reveal' ) ) {
			return true;
		}

	}
}