<?php
/**
 * Header Logo
 *
 * @package Total
 * @subpackage Header Functions
 * @author Alexander Clarke
 * @copyright Copyright (c) 2014, Symple Workz LLC
 * @link http://www.wpexplorer.com
 * @since Total 1.1
 */

/**
 * Returns header logo img url
 *
 * @since Total 1.53
 */
if ( ! function_exists( 'wpex_header_logo_img' ) ) {
	function wpex_header_logo_img() {

		// Get logo img from admin panel
		$logo_img = wpex_option( 'custom_logo', false, 'url' );

		// If logo URL isn't empty return the logo
		if ( '' != $logo_img ) {
			return $logo_img;
		}

		// Otherwise if logo is empty return nothing
		else {
			return;
		}

	}
}

/**
 * Outputs the header logo HTML
 *
 * @since Total 1.0
 */
if ( ! function_exists( 'wpex_header_logo' ) ) {
	function wpex_header_logo() {

		// Get header style to add to the logo classes
		$header_style = wpex_get_header_style();

		// Logo Classes
		$classes = 'header-'. $header_style .'-logo';

		// Set url for the logo with filter so it can be altered via a child theme
		$site_url = esc_url( home_url( '/' ) );
		$logo_url = apply_filters( 'wpex_logo_url', $site_url );

		// Get logo img
		$logo_img = wpex_header_logo_img();

		// Get overlay image url
		if ( wpex_is_overlay_header_enabled() && '' != get_post_meta( get_the_ID(), 'wpex_custom_logo', true ) ) {
			$overlay_logo = get_post_meta( get_the_ID(), 'wpex_overlay_header_logo', true );
			if ( is_array( $overlay_logo ) && !empty( $overlay_logo['url'] ) ) {
				$overlay_logo = $overlay_logo['url'];
				$classes .= ' has-overlay-logo';
			}
		}

		// Get title for the logo based on the blogname & apply filters for easy customization
		$blogname = get_bloginfo( 'name' );
		$logo_title = apply_filters( 'wpex_logo_title', $blogname );

		// If a logo url exists output the logo
		if ( $logo_url ) { ?>
			<div id="site-logo" class="<?php echo $classes; ?>">
				<?php
				// Display main logo
				if ( '' != $logo_img ) { ?>
					<a href="<?php echo esc_url( $logo_url ); ?>" title="<?php echo $logo_title; ?>" rel="home" class="main-logo">
						<img src="<?php echo esc_url( $logo_img ); ?>" alt="<?php echo $logo_title; ?>" />
					</a>
					<?php
					// Display alternative logo for overlay header
					if ( isset( $overlay_logo ) && '' != $overlay_logo ) { ?>
						<a href="<?php echo esc_url( $logo_url ); ?>" title="<?php echo $logo_title; ?>" rel="home" class="overlay-header-logo">
							<img src="<?php echo esc_url( $overlay_logo ); ?>" alt="<?php echo $logo_title; ?>" />
						</a>
					<?php } ?>
				<?php
				// Display text style logo
				} else { ?>
					<a href="<?php echo $logo_url; ?>" title="<?php echo $logo_title; ?>" rel="home"><?php echo $logo_title; ?></a>
				<?php } ?>
			</div><!-- #site-logo -->
		<?php
		}

	}
}

/**
 * Adds js for the retina logo
 *
 * @since Total 1.1
 */
if ( ! function_exists( 'wpex_retina_logo' ) ) {
	function wpex_retina_logo() {

		// Get logo URL
		$logo_url = wpex_option( 'retina_logo', '', 'url' );

		// Get logo dimensions
		$logo_height = wpex_option( 'retina_logo_height' );
		$logo_height = preg_replace( '#[^0-9]#','',strip_tags( $logo_height ) );
		
		// If there is a logo and a height defined output retina styles
		if ( '' != $logo_url && '' != $logo_height) {
			$output = '<!-- Retina Logo -->
			<script type="text/javascript">
				jQuery(function($){
					if (window.devicePixelRatio == 2) {
						$("#site-logo img").attr("src", "'. $logo_url .'");
						$("#site-logo img").css("height", "'. $logo_height .'");
					}
				});
			</script>';
			$output =  preg_replace( '/\s+/', ' ', $output );
			echo $output;
		}

	}
}
add_action( 'wp_head', 'wpex_retina_logo' );