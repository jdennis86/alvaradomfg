<?php
/**
 * Adds custom classes to the header container
 *
 * @package Total
 * @subpackage Header Functions
 * @author Alexander Clarke
 * @copyright Copyright (c) 2014, Symple Workz LLC
 * @link http://www.wpexplorer.com
 * @since Total 1.53
 */

if ( ! function_exists( 'wpex_header_classes' ) ) { 
	function wpex_header_classes() {

		// Get header style
		$header_style = wpex_get_header_style();

		// Check if is mobile
		$is_mobile = wp_is_mobile();

		// Setup classes array
		$classes = array();

		// Clearfix class
		$classes['clr'] = 'clr';

		// Main header style
		$classes['header_style'] = 'header-'. $header_style;

		// Sticky Header
		if ( !$is_mobile && wpex_option( 'fixed_header', '1' ) && 'one' == $header_style ) {
			$classes['fixed_scroll'] = 'fixed-scroll';
		}

		// Fixed Header style
		if ( wpex_is_overlay_header_enabled() ) {

			// Remove fixed scroll class
			unset( $classes['fixed_scroll'] );

			// Add overlay header class
			$classes['overlay_header'] = 'overlay-header';

			// Add a fixed class for the overlay-header style only
			if ( !$is_mobile && wpex_option( 'fixed_header', '1' ) ) {
				$classes['fix_on_scroll'] = 'fix-on-scroll';
			}

			// Add overlay header style clss
			if ( '' != get_post_meta( get_the_ID(), 'wpex_overlay_header_style', true ) ) {
				$overlay_style = get_post_meta( get_the_ID(), 'wpex_overlay_header_style', true );
			} else {
				$overlay_style = 'light';	
			}
			$classes['overlay_header_style'] = $overlay_style .'-style';
		}
		
		// Apply filters
		$classes = apply_filters( 'wpex_header_classes', $classes );

		// Echo classes as space seperated classes
		echo implode( ' ', $classes );

	}
}