<?php
/**
 * Header Menu
 *
 * @package Total
 * @subpackage Menu Functions
 * @author Alexander Clarke
 * @copyright Copyright (c) 2014, Symple Workz LLC
 * @link http://www.wpexplorer.com
 * @since Total 1.0
 */

/**
 * Checks for custom menus
 *
 * @since Total 1.3
 */
if ( ! function_exists( 'wpex_custom_menu' ) ) {
	function wpex_custom_menu( $menu = false ) {
		if ( is_singular( 'page' ) ) {
			global $post;
			$post_id = $post->ID;
			if ( get_post_meta( $post_id, 'wpex_custom_menu', true ) && 'default' != get_post_meta( $post_id, 'wpex_custom_menu', true ) ) {
				$menu = get_post_meta( $post_id, 'wpex_custom_menu', true );
			}
		}
		return apply_filters( 'wpex_custom_menu', $menu );
	}
}


/**
 * Outputs the main header menu
 *
 * @since Total 1.0
 */
if ( ! function_exists( 'wpex_header_menu' ) ) {
	function wpex_header_menu() {
		
		// Vars
		$wrap_classes = $classes = '';
		$header_style = wpex_get_header_style();
		$header_height = intval( wpex_option( 'header_height' ) );
		$woo_icon = wpex_option( 'woo_menu_icon', '1' );

		// Main + Header Style Wrap Classes
		$wrap_classes .= 'clr navbar-style-'. $header_style;

		// Add the fixed-nav class if the fixed header option is enabled
		if ( 'one' != $header_style && wpex_option( 'fixed_header', '1' ) ){
			$wrap_classes .= ' fixed-nav';
		}

		// Add fixed height class if it's header style one and a header height is defined in the admin
		if ( 'one' == $header_style && '' != $header_height ) {
			if ( $header_height && '0' != $header_height && 'auto' != $header_height ) {
				$wrap_classes .= ' nav-custom-height';
			}
		}

		// Add special class if the dropdown top border option in the admin is enabled
		if (  wpex_option( 'menu_dropdown_top_border', '1' ) ) {
			$wrap_classes .= ' nav-dropdown-top-border';
		}

		// Add the container div for header style's two and three
		if ( 'two' == $header_style || 'three' == $header_style ) {
			$classes .= 'container';
		}

		// Add classes if the search setting is enabled
		if ( wpex_option( 'main_search', '1' ) ) {
			$classes .= ' site-navigation-with-search';
			if ( class_exists( 'Woocommerce' ) && $woo_icon ) {
				$classes .= ' site-navigation-with-cart-icon';
			} elseif ( class_exists('Woocommerce') && ! $woo_icon) {
				$classes .= ' site-navigation-without-cart-icon';
			} else {
				$classes .= ' site-navigation-without-cart-icon';
			}
		}

		// Before main menu hook
		wpex_hook_main_menu_before(); ?>
		
		<div id="site-navigation-wrap" class="<?php echo $wrap_classes; ?>">
			<nav id="site-navigation" class="navigation main-navigation clr <?php echo $classes; ?>" role="navigation">
				<?php
				// Top menu hook
				wpex_hook_main_menu_top();

				// Menu Location
				$menu_location = apply_filters( 'wpex_main_menu_location', 'main_menu' );

				// Custom Menu
				$menu = wpex_custom_menu();

				// Display main menu
				if ( $menu ) {
					wp_nav_menu( array(
						'menu'				=> $menu,
						'theme_location'	=> $menu_location,
						'menu_class'		=> 'dropdown-menu sf-menu',
						'fallback_cb'		=> false,
						'walker'			=> new WPEX_Dropdown_Walker_Nav_Menu()
					) );
				} else {
					wp_nav_menu( array(
						'theme_location'	=> $menu_location,
						'menu_class'		=> 'dropdown-menu sf-menu',
						'walker'			=> new WPEX_Dropdown_Walker_Nav_Menu(),
						'fallback_cb'		=> false,
					) );
				}
				// Botttom main menu hook
				wpex_hook_main_menu_bottom(); ?>
			</nav><!-- #site-navigation -->
		</div><!-- #site-navigation-wrap -->
		
		<?php
		// After main menu hook
		wpex_hook_main_menu_after();
		
	}
}
