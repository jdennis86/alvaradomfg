<?php
/**
 * Core functions for blog entry output
 *
 * @package Total
 * @subpackage Blog Functions
 * @author Alexander Clarke
 * @copyright Copyright (c) 2014, Symple Workz LLC
 * @link http://www.wpexplorer.com
 * @since Total 1.0
 */

/**
 * Display author avatar for entries
 *
 * @since Total 1.0
 */
if ( ! function_exists( 'wpex_post_entry_author_avatar' ) ) {
	function wpex_post_entry_author_avatar() {
		if ( !wpex_post_entry_author_avatar_enabled() ) {
			return;
		} ?>
		<div class="blog-entry-author-avatar">
			<a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>" title="<?php echo __( 'Visit Author Page', 'wpex' ); ?>"><?php echo get_avatar( get_the_author_meta( 'user_email' ), apply_filters( 'wpex_author_bio_avatar_size', 74 ) ) ?></a>
		</div>
	<?php }
}

/**
 * Displays the blog entry image
 *
 * @since Total 1.41
 */
if ( ! function_exists( 'wpex_blog_entry_image' ) ) {
	function wpex_blog_entry_image() {
		if ( has_post_thumbnail() ) {
			$image = wpex_image( 'array' );
			// Lightbox
			if ( wpex_option( 'blog_entry_image_lightbox' ) ) {
				$lightbox_url = wpex_image_resize( wp_get_attachment_url( get_post_thumbnail_id() ), 1500, 9999, false ); ?>
				<a href="<?php echo $lightbox_url; ?>" title="<?php echo esc_attr( the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark" class="blog-entry-media-link <?php wpex_img_animation_classes(); ?> wpex-lightbox" data-type="image"><img src="<?php echo $image['url']; ?>" alt="<?php echo the_title(); ?>" width="<?php echo $image['width']; ?>" height="<?php echo $image['height']; ?>" /></a>
			<?php }
			// Standard link to post
			else { ?>
				<a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark" class="blog-entry-media-link <?php wpex_img_animation_classes(); ?>"><img src="<?php echo $image['url']; ?>" alt="<?php echo the_title(); ?>" width="<?php echo $image['width']; ?>" height="<?php echo $image['height']; ?>" /></a>
			<?php } ?>
	<?php }
	}
}

/**
 * Displays the blog entry gallery
 *
 * @since Total 1.41
 */
if ( ! function_exists( 'wpex_blog_entry_gallery' ) ) {
	function wpex_blog_entry_gallery() {
		// Get attachments
		$attachments = wpex_get_gallery_ids();
		// Show featured image for password-protected post
		if ( post_password_required() ) {
			return wpex_blog_entry_image();
		// If there aren't attachments return nothing
		} elseif( empty( $attachments ) ) {
			return;
		} ?>
		<div class="gallery-format-post-slider-wrap clr">
			<div class="gallery-format-post-slider flexslider-container">
					<div class="flexslider">
						<ul class="slides <?php if ( wpex_gallery_is_lightbox_enabled() == 'on' ) echo 'wpex-gallery-lightbox'; ?>">
							<?php foreach ( $attachments as $attachment ) {
								// Get image alt tag
								$attachment_alt = strip_tags( get_post_meta( $attachment, '_wp_attachment_image_alt', true ) ); ?>
								<li class="slide" data-thumb="<?php echo wpex_image_resize( wp_get_attachment_url( $attachment ), '100', '100', true ); ?>">
									<?php
									// Display image with lightbox
									if ( wpex_gallery_is_lightbox_enabled() == 'on' ) {
										$lightbox_url = wpex_image_resize( wp_get_attachment_url( $attachment ), 1500, 9999, false ); ?>
										<a href="<?php echo $lightbox_url; ?>" title="<?php echo $attachment_alt; ?>" data-title="<?php echo $attachment_alt; ?>" data-type="image">
											<img src="<?php echo wpex_image( 'url', $attachment ); ?>" alt="<?php echo $attachment_alt; ?>" />
										</a>
									<?php } else {
										// Lightbox is disabled, only show image ?>
										<img src="<?php echo wpex_image( 'url', $attachment ); ?>" alt="<?php echo $attachment_alt; ?>" />
									<?php } ?>
								</li>
							<?php } ?>
						</ul><!-- .slides -->
					</div><!-- .flexslider -->
			</div><!-- .flexslider-container -->
		</div><!-- .gallery-format-post-slider-wrap -->
	<?php
	}
}

/**
 * Displays the blog entry video
 *
 * @since Total 1.41
 */
if ( ! function_exists( 'wpex_blog_entry_video' ) ) {
	function wpex_blog_entry_video() {
		// Show featured image for password-protected post
		if ( post_password_required() ) {
			return wpex_blog_entry_image();
		}
		// Display oembeded video
		if ( '' != get_post_meta( get_the_ID(), 'wpex_post_oembed', true ) ) { ?>
			<div class="blog-entry-video responsive-video-wrap"><?php echo wp_oembed_get( wpex_post_video_url() ); ?></div>
		<?php }
		elseif ( wpex_post_video_url() ) { ?>
			<div class="blog-entry-video"><?php echo apply_filters( 'the_content', wpex_post_video_url() ); ?></div>
		<?php }
		// Display post thumbnail
		else{
			return wpex_blog_entry_image();
		}
	}
}

/**
 * Displays the blog entry audio
 *
 * @since Total 1.41
 */
if ( ! function_exists( 'wpex_blog_entry_audio' ) ) {
	function wpex_blog_entry_audio() {
		if ( has_post_thumbnail() ) {
			$image = wpex_image( 'array' ); ?>
			<a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark" class="blog-entry-img-link">
				<img src="<?php echo $image['url']; ?>" alt="<?php echo the_title(); ?>" width="<?php echo $image['width']; ?>" height="<?php echo $image['height']; ?>" />
				<div class="blog-entry-music-icon-overlay"><span class="fa fa-music"></span></div>
			</a>
	<?php }
	}
}

/**
 * Displays the blog entry media
 *
 * @since Total 1.41
 */
if ( ! function_exists( 'wpex_blog_entry_media' ) ) {
	function wpex_blog_entry_media() { ?>
		<div class="blog-entry-media clr">
			<?php
			// Get post format
			$format = get_post_format();
			// Display Audio
			if ( 'audio' == $format ) {
				wpex_blog_entry_audio();
			}
			// Display Gallery
			elseif ( 'gallery' == $format ) {
				wpex_blog_entry_gallery();
			}
			// Display Video
			elseif( 'video' == $format ) {
				wpex_blog_entry_video();
			}
			// Display Featured Image
			else {
				wpex_blog_entry_image();
			} ?>
		</div><!-- .blog-entry-media -->
	<?php }
}

/**
 * Displays the blog entry title
 *
 * @since Total 1.41
 */
if ( ! function_exists( 'wpex_blog_entry_title' ) ) {
	function wpex_blog_entry_title() { ?>
		<h2 class="blog-entry-title"><a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
	<?php }
}

/**
 * Displays the blog entry Header
 *
 * @since Total 1.53
 */
if ( ! function_exists( 'wpex_blog_entry_header' ) ) {
	function wpex_blog_entry_header() { ?>
		<header class="clr <?php if ( wpex_post_entry_author_avatar_enabled() ) { echo 'header-with-avatar'; } ?>">
			<?php
			// Display entry title
			wpex_blog_entry_title();
			// Displays the post entry author avatar
			wpex_post_entry_author_avatar();
			// Display post meta - see functions/post-meta.php
			wpex_post_meta(); ?>
		</header>
	<?php }
}

/**
 * Displays the blog entry content
 *
 * @since Total 1.41
 */
if ( ! function_exists( 'wpex_blog_entry_content' ) ) {
	function wpex_blog_entry_content() { ?>
		<div class="blog-entry-excerpt entry">
			<?php
			// Display excerpt if auto excerpts are enavled in the admin
			if ( wpex_option( 'blog_exceprt', '1' ) ) {
				// Get excerpt length & output excerpt
				// See functions/excerpts.php
				wpex_excerpt( wpex_excerpt_length() );
			} else {
			// Display full content
				the_content( __( 'Continue reading <span class="meta-nav">&rarr;</span>', 'wpex' ) );
			} ?>
		</div><!-- .blog-entry-excerpt -->
	<?php }
}

/**
 * Output the blog entry based on the theme options builder
 *
 * @since Total 1.53
 */
if ( ! function_exists( 'wpex_blog_entry_display' ) ) {
	function wpex_blog_entry_display() { ?>
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<div class="blog-entry-inner clr">
				<?php
				// Thumbnail entry style does not support entry builder
				if ( 'thumbnail-entry-style' == wpex_blog_entry_style() ) {
					wpex_blog_entry_media(); ?>
					<div class="blog-entry-content clr">
						<?php
						wpex_blog_entry_header();
						wpex_blog_entry_content();
						wpex_post_readmore_link(); ?>
					</div><!-- blog-entry-content -->
				<?php } else {
					// Loop through composer blocks and output layout
					$blocks = array(
						'enabled'	=> array(
							'featured_media'	=> __( 'Featured Media','wpex' ),
							'title_meta'		=> __( 'Title & Meta','wpex' ),
							'excerpt_content'	=> __( 'Excerpt','wpex' ),
							'readmore'			=> __( 'Read More','wpex' ),
						),
						'disabled'		=> array(
							'post_tags'	=> __( 'Post Tags','wpex' ),
						),
					);
					$blocks = wpex_option( 'blog_entry_composer', $blocks );
					$blocks = $blocks['enabled'];
					foreach ( $blocks as $key=>$value ) :
						switch ( $key ) {
							// Display entry media
							case 'featured_media':
							wpex_blog_entry_media();
							break;
							// Blog Entry Header
							case 'title_meta':
							wpex_blog_entry_header();
							break;
							// Entry Excerpt/Content
							case 'excerpt_content':
							wpex_blog_entry_content();
							break;
							// Read more link
							case 'readmore':		
							wpex_post_readmore_link();
							break;
						}
					endforeach; ?>
				<?php } ?>
			</div><!-- .blog-entry-inner -->
		</article><!-- .blog-entry -->
	<?php }
}