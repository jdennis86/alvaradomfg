<?php
/**
 * Add post type classes to entries
 *
 * @package Total
 * @subpackage functions
 * @author Alexander Clarke
 * @copyright Copyright (c) 2014, Symple Workz LLC
 * @link http://www.wpexplorer.com
 * @since Total 1.0
 */

/**
 * Hooks into the post_class filter to add extra classes for post entries
 * Used for standard post type entries only
 *
 * @link http://codex.wordpress.org/Function_Reference/post_class
 * @since 1.0
 */
if ( ! function_exists( 'wpex_post_entry_classes' ) ) {
	function wpex_post_entry_classes( $classes ) {

		// Return on search and singular
		if ( is_search() || is_singular() ) {
			return $classes;
		}
		
		// Post Data
		global $post;
		$post_type = get_post_type( $post->ID );
				
		// Custom class for post types
		if ( 'post' != $post_type ) {
			$theme_post_types = array( 'post', 'portfolio', 'staff', 'testimonials', 'product' );
			if ( !in_array( $post_type, $theme_post_types ) ) {
				// Non Theme post types
				$classes[] = 'custom-post-type-entry';
			} else {
				// Theme post type class
				$classes[] = $post_type .'entry';
			}
			return $classes;
		}
		
		// Blog entry style
		// See framework/blog/blog-functions.php
		$blog_style = wpex_blog_entry_style();
		
		// Main Classes
		$classes[] = 'blog-entry clr';
		
		// No Featured Image Class
		if ( !has_post_thumbnail( $post->ID ) && '' == get_post_meta( get_the_ID(), 'wpex_post_self_hosted_shortcode', true ) && '' == get_post_meta( get_the_ID(), 'wpex_post_oembed', true ) ) {
			$classes[] = 'no-featured-image';
		}

		// Add columns for grid style entries
		if ( $blog_style == 'grid-entry-style' ) {
			$classes[] = 'col';
			$classes[] = wpex_grid_class( wpex_blog_entry_columns() );
		}

		// Blog entry style
		$classes[] = $blog_style;
		
		// Return classes	
		return $classes;
		
	}
}
add_filter( 'post_class', 'wpex_post_entry_classes' );