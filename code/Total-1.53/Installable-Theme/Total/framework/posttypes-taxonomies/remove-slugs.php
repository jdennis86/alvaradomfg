<?php
/**
 * Used to remove slugs from custom post type URLs
 *
 * @package Total
 * @subpackage functions
 * @author Alexander Clarke
 * @copyright Copyright (c) 2014, Symple Workz LLC
 * @link http://www.wpexplorer.com
 * @since Total 1.0
 */


function wpex_remove_cpt_slug( $post_link, $post, $leavename ) {
	$post_types = wpex_active_post_types();
	if ( ! in_array( $post->post_type, $post_types ) || 'publish' != $post->post_status ) {
		return $post_link;
	}
	foreach ( $post_types as $post_type ) {
		$post_link = str_replace( '/'. wpex_option( $post_type .'_slug' ) .'/', '/', $post_link );
	}
	return $post_link;
}
function wpex_parse_request_tricksy( $query ) {
	// Get theme post types
	$post_types = wpex_active_post_types();
	// Only noop the main query
	if ( ! $query->is_main_query() )
		return;
	// Only noop our very specific rewrite rule match
	if ( 2 != count( $query->query ) || ! isset( $query->query['page'] ) ) {
		return;
	}
	// 'name' will be set if post permalinks are just post_name, otherwise the page rule will match
	if ( ! empty( $query->query['name'] ) ) {
		$array = array( 'post', 'page' );
		$array = array_merge( $array, $post_types );
		$query->set( 'post_type', $array );
	}
}
if ( wpex_option( 'remove_posttype_slugs' ) ) {
	add_filter( 'post_type_link', 'wpex_remove_cpt_slug', 10, 3 );
	add_action( 'pre_get_posts', 'wpex_parse_request_tricksy' );
}