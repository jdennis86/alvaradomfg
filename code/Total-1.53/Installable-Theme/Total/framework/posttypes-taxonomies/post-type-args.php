<?php
/**
 * Used to re-brand built-in custom post types
 *
 * @package Total
 * @subpackage functions
 * @author Alexander Clarke
 * @copyright Copyright (c) 2014, Symple Workz LLC
 * @link http://www.wpexplorer.com
 * @since Total 1.0
 */

/**
 * Portfolio Post Type
 *
 * @since 1.0
 */
if ( ! function_exists( 'wpex_custom_portfolio_args' ) ) {
	function wpex_custom_portfolio_args( $args ) {
		// Labels
		$option = wpex_option( 'portfolio_labels' );
		if ( !empty( $option ) && 'Portfolio' != $option ) {
			$args['labels']['name']					= $option;
			$args['labels']['singular_name']		= $option;
			$args['labels']['add_new']				= __( 'Add New', 'wpex' );
			$args['labels']['add_new_item']			= __( 'Add New Item', 'wpex' );
			$args['labels']['edit_item']			= __( 'Edit Item', 'wpex' );
			$args['labels']['new_item']				= __( 'New Item', 'wpex' );
			$args['labels']['view_item']			= __( 'View Item', 'wpex' );
			$args['labels']['search_items']			= __( 'Search Items', 'wpex' );
			$args['labels']['not_found']			= __( 'No Items Found', 'wpex' );
			$args['labels']['not_found_in_trash']	= __( 'No Items Found In Trash', 'wpex' );
		}
		// Slug
		$option = wpex_option( 'portfolio_slug', 'portfolio-item' );
		if ( !empty( $option ) && $option ) {
			$args['rewrite'] = array( "slug" => $option );
		}
		// Admin Icon
		$option = wpex_option( 'portfolio_admin_icon' );
		if ( !empty( $option ) && $option && !is_array( $option ) ) {
			$args['menu_icon'] = 'dashicons-'. $option;
		}
		// Search
		$option = wpex_option( 'portfolio_search', '1' );
		if ( ! $option ) {
			$args['exclude_from_search'] = true;
		}
		// Return args
		return $args;
	}
}
add_filter( 'wpex_portfolio_args', 'wpex_custom_portfolio_args' );

/**
 * Staff Post Type
 *
 * @since 1.0
 */
if ( ! function_exists( 'wpex_custom_staff_args' ) ) {
	function wpex_custom_staff_args( $args ) {
		// Labels
		$option = wpex_option( 'staff_labels' );
		if ( !empty( $option ) && 'Staff' != $option ) {
			$args['labels']['name']					= $option;
			$args['labels']['singular_name']		= $option;
			$args['labels']['add_new']				= __( 'Add New', 'wpex' );
			$args['labels']['add_new_item']			= __( 'Add New Item', 'wpex' );
			$args['labels']['edit_item']			= __( 'Edit Item', 'wpex' );
			$args['labels']['new_item']				= __( 'New Item', 'wpex' );
			$args['labels']['view_item']			= __( 'View Item', 'wpex' );
			$args['labels']['search_items']			= __( 'Search Items', 'wpex' );
			$args['labels']['not_found']			= __( 'No Items Found', 'wpex' );
			$args['labels']['not_found_in_trash']	= __( 'No Items Found In Trash', 'wpex' );
		}
		// Slug
		$option = wpex_option( 'staff_slug' );
		if ( !empty( $option ) && 'staff-member' != $option ) {
			$args['rewrite'] = array( "slug" => $option );
		}
		// Admin Icon
		$option = wpex_option( 'staff_admin_icon' );
		if ( !empty( $option ) && $option && !is_array( $option ) ) {
			$args['menu_icon'] = 'dashicons-'. $option;
		}
		// Search
		$option = wpex_option( 'staff_search', '1' );
		if ( ! $option ) {
			$args['exclude_from_search'] = true;
		}
		// Return args
		return $args;
	}
}
add_filter( 'wpex_staff_args', 'wpex_custom_staff_args' );

/**
 * Testimonials Post Type
 *
 * @since 1.0
 */
if ( ! function_exists( 'wpex_custom_testimonials_args' ) ) {
	function wpex_custom_testimonials_args( $args ) {
		// Labels
		$option = wpex_option( 'testimonials_labels' );
		if ( !empty( $option ) && 'Testimonials' != $option ) {
			$args['labels']['name']					= $option;
			$args['labels']['singular_name']		= $option;
			$args['labels']['add_new']				= __( 'Add New', 'wpex' );
			$args['labels']['add_new_item']			= __( 'Add New Item', 'wpex' );
			$args['labels']['edit_item']			= __( 'Edit Item', 'wpex' );
			$args['labels']['new_item']				= __( 'New Item', 'wpex' );
			$args['labels']['view_item']			= __( 'View Item', 'wpex' );
			$args['labels']['search_items']			= __( 'Search Items', 'wpex' );
			$args['labels']['not_found']			= __( 'No Items Found', 'wpex' );
			$args['labels']['not_found_in_trash']	= __( 'No Items Found In Trash', 'wpex' );
		}
		// Slug
		$option = wpex_option( 'testimonials_slug' );
		if ( !empty( $option ) && 'testimonial' != $option ) {
			$args['rewrite'] = array( "slug" => $option );
		}
		// Admin Icon
		$option = wpex_option( 'testimonials_admin_icon' );
		if ( !empty( $option ) && $option && !is_array( $option ) ) {
			$args['menu_icon'] = 'dashicons-'. $option;
		}
		// Search
		$option = wpex_option( 'testimonials_search', '1' );
		if ( ! $option ) {
			$args['exclude_from_search'] = true;
		}
		// Return args
		return $args;
	}
}
add_filter( 'wpex_testimonials_args', 'wpex_custom_testimonials_args' );