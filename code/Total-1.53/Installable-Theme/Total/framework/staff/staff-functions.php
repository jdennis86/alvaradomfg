<?php
/**
 * Useful global functions for the staff
 *
 * @package Total
 * @subpackage Staff Functions
 * @author Alexander Clarke
 * @copyright Copyright (c) 2014, Symple Workz LLC
 * @link http://www.wpexplorer.com
 * @since Total 1.0
 */

/**
 * Returns correct classes for the staff wrap
 *
 * @since Total 1.53
 * @return var $classes
 */
if ( ! function_exists( 'wpex_get_staff_wrap_classes' ) ) {
	function wpex_get_staff_wrap_classes() {
		$classes = array( 'wpex-row', 'clr' );
		$classes[] = 'staff-'. wpex_option( 'staff_archive_grid_style', 'fit-rows' );
		return implode( " ",$classes );
	}
}

/**
 * Checks if match heights are enabled for the staff
 *
 * @since Total 1.53
 * @return bool
 */
if ( ! function_exists( 'wpex_staff_match_height' ) ) {
	function wpex_staff_match_height() {
		if ( 'fit-rows' == wpex_option( 'staff_archive_grid_style', 'fit-rows' )
			&& wpex_option( 'staff_archive_grid_equal_heights' ) ) {
			if ( '1' == wpex_option( 'staff_entry_columns', '4' ) ) {
				return false;
			} else {
				return true;
			}
		} else {
			return false;
		}
	}
}

/**
 * Staff Overlay
 *
 * @since Total 1.0
 */
if ( ! function_exists( 'wpex_get_staff_overlay' ) ) {
	function wpex_get_staff_overlay( $id=NULL ) {
		$post_id = $id ? $id : get_the_ID();
		$position = get_post_meta( get_the_ID(), 'wpex_staff_position', true );
		$output='';
		if ( empty( $position) || $position == '' ) {
			return;
		} ?>
		<div class="staff-entry-position">
			<span><?php echo $position; ?></span>
		</div><!-- .staff-entry-position -->
		<?php return $output;
	}
}

/**
 * Outputs the staff social options
 *
 * @since Total 1.0
 */
if ( ! function_exists( 'wpex_get_staff_social' ) ) {
	function wpex_get_staff_social( $atts = NULL ) {

		extract( shortcode_atts( array(
			'link_target'	=> 'blank',
		),
		$atts ) );

		global $post;
		$post_id = $post->ID;
		$output='';
		$profiles = array(
			'twitter'		=> 'Twitter',
			'facebook'		=> 'Facebook',
			'google-plus'	=> 'Google Plus',
			'dribbble'		=> 'Dribbble',
			'linkedin'		=> 'LinkedIn',
			'skype'			=> 'Skype',
			'email'			=> __( 'Email', 'wpex' ),
			'website'		=> __( 'Website', 'wpex' ),
		);

		ob_start();

		// Do not display if disabled for the archives
		if ( is_tax() && ! wpex_option( 'staff_entry_social', '1' ) ) {
			return;
		} ?>

			<div class="staff-social clr">
				<?php foreach ( $profiles as $key => $value ) {
					$meta = get_post_meta( $post_id, 'wpex_staff_'. $key, true );
					if ( 'email' == $key ) {
						$key = 'envelope';
					}
					if ( 'website' == $key ) {
						$key = 'external-link-square';
					}
					if ( $meta ) { ?>
						<a href="<?php echo esc_url($meta); ?>" title="<?php echo $value; ?>" class="staff-<?php echo $key; ?> tooltip-up" target="_<?php echo $link_target; ?>">
							<span class="fa fa-<?php echo $key; ?>"></span>
						</a>
					<?php }
				} ?>
			</div><!-- .staff-social -->

		<?php
		return ob_get_clean();
	}
}
add_shortcode( 'staff_social', 'wpex_get_staff_social' );