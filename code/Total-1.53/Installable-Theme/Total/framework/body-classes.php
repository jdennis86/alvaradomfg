<?php
/**
 * Adds classes to the body tag for various page/post layout styles
 *
 * @package Total
 * @subpackage functions
 * @author Alexander Clarke
 * @copyright Copyright (c) 2014, Symple Workz LLC
 * @link http://www.wpexplorer.com
 * @since Total 1.0
 */

if ( !function_exists( 'wpex_body_classes' ) ) {
	function wpex_body_classes( $classes ) {
		
		// WPExplorer class
		$classes[] = 'wpex-theme';

		// Responsive
		if ( wpex_option( 'responsive', '1' ) ) {
			$classes[] = 'wpex-responsive';
		}
		
		// Add skin to body classes
		if ( function_exists( 'wpex_active_skin') ) {
			if ( wpex_active_skin() ) {
				$classes[] = 'theme-'. wpex_active_skin();
			}
		}
		
		// Page with Slider or header
		if ( is_singular() ) {
			global $post;
			$post_id = $post->ID;
			$slider = get_post_meta( $post_id, 'wpex_post_slider_shortcode', true );
			$title_style = get_post_meta( $post_id, 'wpex_post_title_style', true );
			if ( $slider ) {
				$classes[] = 'page-with-slider';
			}
			if ( $title_style == 'background-image' ) {
				$classes[] = 'page-with-background-title';
			}
		}
		
		// Layout Style
		if ( function_exists( 'wpex_main_layout' ) ) {
			$classes[] = wpex_main_layout() .'-main-layout';
		}

		// Content layout
		if ( function_exists( 'wpex_get_post_layout_class' ) ) {
			$classes[] = 'content-'. wpex_get_post_layout_class();
		}
		
		// Remove header bottom margin
		if ( is_singular() ) {
			global $post;
			if ( 'on' == get_post_meta( $post->ID, 'wpex_disable_header_margin', true ) ) {
				$classes[] = 'no-header-margin';
			}
		}

		// Check if breadcrumbs are enabled
		if ( function_exists( 'wpex_breadcrumbs_enabled' )&& wpex_breadcrumbs_enabled() && 'default' == wpex_option( 'breadcrumbs_position', 'default' ) ) {
			$classes[] = 'has-breadcrumbs';
		}

		// Shrink fixed header
		if ( wpex_option( 'shink_fixed_header', '1' ) && 'one' == wpex_option( 'header_style', '1' ) ) {
			$classes[] = 'shrink-fixed-header';
		}

		// Single Post cagegories
		if ( is_singular( 'post' ) ) {
			global $post;
			$cats = get_the_category($post->ID);
			foreach ( $cats as $c ) {
				$classes[] = 'post-in-category-'. $c->category_nicename;
			}
		}
		
		// WooCommerce
		if ( class_exists('Woocommerce') ) {
			if ( wpex_option( 'woo_shop_slider' ) !== '' && is_shop() ) {
				$classes[] = 'page-with-slider';
			}
			if ( wpex_option( 'woo_shop_title', '1' ) !== '1' && is_shop() ) {
				$classes[] = 'page-without-title';
			}
		}

		// Widget Icons
		if ( wpex_option('widget_icons', '1' ) == '1' ) {
			$classes[] = 'sidebar-widget-icons';
		}

		// Mobile
		if ( wp_is_mobile() ) {
			$classes[] = 'is-mobile';
		}

		// Overlay header style
		if ( function_exists( 'wpex_is_overlay_header_enabled' ) && wpex_is_overlay_header_enabled() ) {
			$classes[] = 'has-overlay-header';
		}

		// Footer reveal
		if( function_exists( 'wpex_footer_reveal_enabled' ) && wpex_footer_reveal_enabled() ) {
			$classes[] = 'footer-has-reveal';
		}
		
		return $classes;
	}
}
add_filter( 'body_class', 'wpex_body_classes' );