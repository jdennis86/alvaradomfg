<?php
/**
 * Updates old param values to use new VC params.
 *
 * @package Total
 * @subpackage Visual Composer
 * @author Alexander Clarke
 * @copyright Copyright (c) 2014, Symple Workz LLC
 * @link http://www.wpexplorer.com
 * @since Total 1.4
 */

/** Work In Progress: http://kb.wpbakery.com/index.php?title=Update_single_param_values **/