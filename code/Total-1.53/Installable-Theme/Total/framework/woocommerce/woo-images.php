<?php
/**
 * Change default Woo Image sizes
 *
 * @package Total
 * @subpackage WooCommerce
 * @author Alexander Clarke
 * @copyright Copyright (c) 2014, Symple Workz LLC
 * @link http://www.wpexplorer.com
 * @since Total 1.0
 */

// We don't need this to run on the front-end
if ( ! is_admin() ) {
	return;
}

global $pagenow;

if ( isset( $_GET['activated'] ) && 'themes.php' == $pagenow ) {
	add_action( 'init', 'wpex_woocommerce_image_dimensions', 1 );
}

function wpex_woocommerce_image_dimensions() {
	
	$catalog = array(
		'width' 	=> '9999',
		'height'	=> '9999',
		'crop'		=> 0
	);
	
	$single = array(
		'width' 	=> '9999',
		'height'	=> '9999',
		'crop'		=> 0
	);

	$thumbnail = array(
		'width' 	=> '100',
		'height'	=> '100',
		'crop'		=> 1
	);
	
	update_option( 'shop_catalog_image_size', $catalog );
	update_option( 'shop_single_image_size', $single );
	update_option( 'shop_thumbnail_image_size', $thumbnail );
	
}