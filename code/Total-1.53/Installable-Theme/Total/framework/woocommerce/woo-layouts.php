<?php
/**
 * Useful functions for latering the WooCommerce layouts
 *
 * @package Total
 * @subpackage WooCommerce
 * @author Alexander Clarke
 * @copyright Copyright (c) 2014, Symple Workz LLC
 * @link http://www.wpexplorer.com
 * @since Total 1.0
 */

/**
 * Add classes for the various layouts - with sidebar, without sidebar, full-width
 *
 * @since Total 1.0
 */
if ( !function_exists( 'wpex_woo_layout_class' ) ) {
	function wpex_woo_layout_class() {
		
		$classes=array();
		
		// Main Shop & archives
		if ( is_shop() || is_product_category() || is_product_tag() ) {
			$classes[] = wpex_option( 'woo_shop_layout', 'full-width' );
		}
		
		// Single Products
		if ( is_singular( 'product' ) ) {
			if ( '' != get_post_meta( get_the_ID(), 'wpex_post_layout', true ) ) {
				$classes[] = get_post_meta( get_the_ID(), 'wpex_post_layout', true );
			} else {
				$classes[] = wpex_option( 'woo_product_layout', 'full-width' );
			}
		}
		
		// Filter for devs
		$classes = apply_filters( 'wpex_woo_wrap_classes', $classes );
		
		// Ninja work
		$classes = implode( " ", $classes );
		
		// Return classes
		return $classes;
		
	}
}

/**
 * Checks if the single product posts should have a sidebar
 *
 * @since Total 1.0
 */
if ( !function_exists( 'wpex_woo_sidebar' ) ) {
	function wpex_woo_sidebar() {
		if ( 'left-sidebar' == wpex_woo_layout_class() ) {
			return true;
		} elseif ( 'right-sidebar' == wpex_woo_layout_class() ) {
			return true;
		} else {
			return false;
		}	
	}
}