<?php
/**
 * Used for your standard post entry content and single post media
 *
 * @package	Total
 * @author Alexander Clarke
 * @copyright Copyright (c) 2014, Symple Workz LLC
 * @link http://www.wpexplorer.com
 * @since Total 1.0
 */


// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/******************************************************
 * Single Posts
 * @since 1.0
*****************************************************/

if ( is_singular() ) {

	// Get post image attachments
	$attachments = wpex_get_gallery_ids();

	// Display slider if there are images saved in the DB
	if ( !empty( $attachments ) ) { ?>
		<div id="post-media" class="clr">
			<div class="gallery-format-post-slider-wrap clr">
				<div class="gallery-format-post-slider flexslider-container">
					<div class="flexslider">
						<ul class="slides wpex-gallery-lightbox">
						<?php foreach ( $attachments as $attachment ) :
								// Get image alt tag
								$attachment_alt = strip_tags( get_post_meta( $attachment, '_wp_attachment_image_alt', true ) ); ?>
								<li class="slide" data-thumb="<?php echo wpex_image_resize( wp_get_attachment_url( $attachment ), 100, 100, true ); ?>">
									<?php
									// Display image with lightbox
									if ( 'on' == wpex_gallery_is_lightbox_enabled() ) {
										$lightbox_url = wpex_image_resize( wp_get_attachment_url( $attachment ), 1500, 9999, false ); ?>
										<a href="<?php echo $lightbox_url; ?>" title="<?php echo get_post_field('post_excerpt', $attachment ); ?>" data-type="image"><img src="<?php echo wpex_image( 'url', $attachment ); ?>" alt="<?php echo $attachment_alt; ?>" /></a>
									<?php } else {
										// Lightbox is disabled, only show image ?>
										<img src="<?php echo wpex_image( 'url', $attachment ); ?>" alt="<?php echo $attachment_alt; ?>" />
									<?php } ?>
								</li>
							<?php endforeach; ?>
						</ul><!-- .slides -->
					</div><!-- .flexslider -->
				</div><!-- .flexslider-container -->
			</div><!-- .gallery-format-post-slider-wrap -->
		</div><!-- #post-media -->
	<?php } ?>

<?php
}
/******************************************************
 * Entries
 * @since 1.0
*****************************************************/
else {
	/**
	 * Holy cow this is simple
	 * Does that mean it's easy to customize as well?
	 * Yes, you are correct!
	 * Visit the link below and you can copy any function to your child theme to override it.
	 *
	 * @link /framework/blog/blog-entry.php for functions
	 */
	wpex_blog_entry_display();
}