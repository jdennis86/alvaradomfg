<?php
/**
 * Used for your video post entry content and single post media
 *
 * @package	Total
 * @author Alexander Clarke
 * @copyright Copyright (c) 2014, Symple Workz LLC
 * @link http://www.wpexplorer.com
 * @since Total 1.0
 */


// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


/******************************************************
 * Single Posts
 * @since 1.0
*****************************************************/

if ( is_singular() ) { ?>
	
	<div id="post-media" class="clr">
		<?php
		// Embeded video
		if ( get_post_meta( get_the_ID(), 'wpex_post_oembed', true ) !== '' ) { ?>
			<div id="blog-post-video" class="responsive-video-wrap"><?php echo wp_oembed_get( wpex_post_video_url() ); ?></div>
		<?php }
		// Self hosted
		elseif ( wpex_post_video_url() ) { ?>
			<div id="blog-post-video"><?php echo apply_filters( 'the_content', wpex_post_video_url() ); ?></div>
		<?php }
		// Featured Image
		elseif ( has_post_thumbnail() ) {
			$wpex_image = wpex_image( 'array' ); ?>
			<a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark" class="blog-entry-img-link"><img src="<?php echo $wpex_image['url']; ?>" alt="<?php echo the_title(); ?>" width="<?php echo $wpex_image['width']; ?>" height="<?php echo $wpex_image['height']; ?>" /></a>
		<?php } ?>
	</div><!-- #post-media -->

<?php
}
/******************************************************
 * Entries
 * @since 1.0
*****************************************************/
else {
	/**
	 * Holy cow this is simple
	 * Does that mean it's easy to customize as well?
	 * Yes, you are correct!
	 * Visit the link below and you can copy any function to your child theme to override it.
	 *
	 * @link /framework/blog/blog-entry.php for functions
	 */
	wpex_blog_entry_display();
}