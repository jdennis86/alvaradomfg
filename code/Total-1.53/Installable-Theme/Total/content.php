<?php
/**
 * Used for your standard post entry content and single post media
 *
 * @package	Total
 * @author Alexander Clarke
 * @copyright Copyright (c) 2014, Symple Workz LLC
 * @link http://www.wpexplorer.com
 * @since Total 1.0
 */


// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


/******************************************************
 * Single Posts
*****************************************************/
if ( is_singular() ) {
	
	// Display featured image if enabled and defined
	if( '1' == wpex_option( 'blog_single_thumbnail', '1' ) && has_post_thumbnail() ) {
		// Get cropped featured image
		$wpex_image = wpex_image( 'array' ); ?>
		<div id="post-media" class="clr">
			<?php
			// Image with lightbox link
			if ( wpex_option( 'blog_post_image_lightbox' ) ) { ?>
				<a href="<?php echo wp_get_attachment_url( get_post_thumbnail_id() ); ?>" title="<?php echo esc_attr( the_title_attribute( 'echo=0' ) ); ?>" class="wpex-lightbox <?php wpex_img_animation_classes(); ?>" data-type="image"><img src="<?php echo $wpex_image['url']; ?>" title="<?php echo esc_attr( the_title_attribute( 'echo=0' ) ); ?>" width="<?php echo $wpex_image['width']; ?>" height="<?php echo $wpex_image['height']; ?>" /></a>
			<?php }
			// No lightbox
			else { ?>
				<img src="<?php echo $wpex_image['url']; ?>" title="<?php echo esc_attr( the_title_attribute( 'echo=0' ) ); ?>" width="<?php echo $wpex_image['width']; ?>" height="<?php echo $wpex_image['height']; ?>" />
			<?php } ?>
			<?php
			// Blog entry caption
			if ( wpex_option( 'blog_thumbnail_caption' ) && wpex_featured_image_caption() ) { ?>
				<div class="post-media-caption clr">
					<?php echo wpex_featured_image_caption(); ?>
				</div>
			<?php } ?>
		</div><!-- #post-media -->
	<?php }

}
/******************************************************
 * Entries
 * @since 1.0
*****************************************************/
else {
	/**
	 * Holy cow this is simple
	 * Does that mean it's easy to customize as well?
	 * Yes, you are correct!
	 * Visit the link below and you can copy any function to your child theme to override it.
	 *
	 * @link /framework/blog/blog-entry.php for functions
	 */
	wpex_blog_entry_display();
}